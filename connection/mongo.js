const mongoose = require("mongoose");
const { config } = require("../config/config");

const user = config.dbUser;
const password = config.dbPassword;
const dbName = config.dbName;
const host = config.dbHost;
const port = config.dbPort;

const mongoConnection = `mongodb+srv://${user}:${password}@${host}/${dbName}?retryWrites=true&w=majority`;

async function connectDB() {
  try {
    await mongoose.connect(mongoConnection, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    });
  } catch (e) {
    console.log("mongodb server error ", e.message);
  }
}

mongoose.connection.on("connected", () => {
  console.log("mongodb server connected!");
});

mongoose.connection.on("disconnected", () => {
  console.log("mongodb server disconnected!");
});

mongoose.connection.on("error", () => {
  console.log("mongodb server error!");
});

mongoose.connection.on("SIGINT", () => {
  mongoose.connection.close(() => {
    console.log("mongodb server SIGINT!");
  });
});

module.exports = connectDB;
