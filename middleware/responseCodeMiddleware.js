//load Response Codes
exports.setResponseCode = (req, res, next) => {
  global.code = require("../files/code");
  next();
};
