/////File for start app
const app = require("./server");

async function main() {
  const port = app.get("port");
  await app.listen(port);
  console.log("###################################################");
  console.log(`####'Api Server Start  http://localhost:${port}'#####`);
  console.log("##################################################");
}

main();
