const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const logSchema = Schema(
  {
    message: {
      type: "String",
    },
    description: {
      type: "String",
    },
    app: {
      type: "String",
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("logApps", logSchema);
