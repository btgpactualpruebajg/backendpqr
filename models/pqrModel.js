const mongoose = require("mongoose");
const pqrTypeModel = require("./pqrTypeModel");

const Schema = mongoose.Schema;

const pqrSchema = Schema(
  {
    settled: {
      type: "Number",
      require: true,
    },
    description: {
      type: "String",
      require: true,
    },
    idPqrType: {
      type: Schema.Types.ObjectId,
      ref: pqrTypeModel,
      require: true,
    },
    response: {
      date: {
        type: "Date",
      },
      message: {
        type: "String",
        require: true,
      },
    },
    claim: {
      created: {
        type: "Date",
      },
      settled: {
        type: "Number",
        require: true,
      },
      messageUser: {
        type: "String",
        require: true,
      },
      messageResponse: {
        type: "String",
        require: true,
      },
      dateReponse: {
        type: "Date",
      },
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("pqr", pqrSchema);
