const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const pqrTypeSchema = Schema(
  {
    name: {
      type: "String",
      require: true,
    },
    slug: {
      type: "String",
      require: true,
    },
    active: {
      type: "Boolean",
      default: true,
    },
    isMain: {
      type: "Boolean",
      default: true,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("pqrType", pqrTypeSchema);
