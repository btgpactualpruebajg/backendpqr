const response = require("./responseHelper");
const logModel = require("../models/logModel");

//if the application is cached we configure the message from here
function errors(err, req, res, next) {
  console.error("[error]", err);
  let message;
  if (err.message.includes("ReferenceError")) {
    saveLogDb(err);
    message = global.lang.general.g101;
  } else message = err.message || "Internal server error";

  const status = err.statusCode || 500;
  response.error(res, message, status);
}

function saveLogDb(err) {
  try {
    const log = new logModel({
      message: err.message,
      description: err.stack,
      app: "ApiPqr",
    });

    log.save((err, data) => {});
  } catch (e) {
    //console.log(e.message)
  }
}

module.exports = errors;
