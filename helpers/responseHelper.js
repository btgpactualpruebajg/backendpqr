exports.success = function (
  res,
  data = "",
  appCode = "g200",
  statusCode = 200
) {
  res.status(statusCode).send({
    error: false,
    status: statusCode,
    appCode: appCode,
    body: data,
  });
};

exports.error = function (res, message, appCode = "g101", statusCode = 200) {
  let errorMessage = message || "Internal server error";
  res.status(statusCode).send({
    error: true,
    status: statusCode,
    appCode: appCode,
    errorMessage: errorMessage,
  });
};
