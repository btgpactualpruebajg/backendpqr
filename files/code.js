var codes = {
  g100: "Welcome Pqr API - up!",
  g101: "Huy! Algo salió mal. ¡Intentar otra vez!",
  g102: "Parámetros inválidos!",
  g103: "Petición o queja no apta para la creación de un reclamo!",
  g104: "Codigo de Petición o queja no Valida!",
  g200: "Ok",
  g201: "Pqr Creado Correctamente!",
  g202: "Reclamo Creada Correctamente!",
};

module.exports = codes;
