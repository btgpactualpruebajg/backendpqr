const express = require("express");
const controller = require("../controllers/pqrTypeController");
const code = require("../middleware/responseCodeMiddleware");

const api = express.Router();

api.post("/", [code.setResponseCode], controller.post);

api.get("/", code.setResponseCode, controller.get);

api.put("/", code.setResponseCode, controller.put);

module.exports = api;
