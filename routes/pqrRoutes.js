const express = require("express");
const controller = require("../controllers/pqrController");
const code = require("../middleware/responseCodeMiddleware");

const api = express.Router();

api.post("/", code.setResponseCode, controller.post);

api.get("/:isClaim/:id?", code.setResponseCode, controller.get);

api.put("/", code.setResponseCode, controller.responsepqr);

api.put("/claim", code.setResponseCode, controller.createClaimPqr);

api.put("/responseclaim", code.setResponseCode, controller.responseClaimPqr);

module.exports = api;
