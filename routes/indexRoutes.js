const express = require("express");
const controller = require("../controllers/indexController");
const responseCode = require("../middleware/responseCodeMiddleware");

const api = express.Router();

api.get("/", responseCode.setResponseCode, controller.getIndex);
module.exports = api;
