const response = require("../helpers/responseHelper");
const { config } = require("../config/config");
const pqrTypeModel = require("../models/pqrTypeModel");

//FUNCTION PUBLIC
function get(req, res) {
  try {
    pqrTypeModel
      .find()
      .sort({ name: "desc" })
      .exec((err, stored) => {
        if (err) {
          return response.error(res, global.code.g101, "g101");
        }

        response.success(res, stored);
      });
  } catch (e) {
    throw new Error(e);
  }
}

function post(req, res) {
  try {
    const data = ({ name, slug, active, isMain } = req.body);
    const modelData = new pqrTypeModel(data);

    modelData.save((err, data) => {
      if (err) {
        return response.error(res, global.code.g101, "g101");
      } else {
        response.success(res, global.code.g200, "g200");
      }
    });
  } catch (e) {
    throw new Error(e);
  }
}

function put(req, res) {
  try {
    const id = req.body.idPqrType;
    pqrTypeModel.findOneAndUpdate(
      { _id: id },
      { $set: req.body },
      { new: true },
      (err, dataupdate) => {
        if (err) return response.error(res, code.g101, "g101");

        if (!dataupdate) return response.error(res, global.code.g101, "g101");

        response.success(res, global.code.g200, "g200");
      }
    );
  } catch (e) {
    throw new Error(e);
  }
}

//FUNCTION PRIVATE

module.exports = {
  get,
  post,
  put,
};
