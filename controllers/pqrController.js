const response = require("../helpers/responseHelper");
const { config } = require("../config/config");
const pqrModel = require("../models/pqrModel");
const moment = require("moment");
var shortid = require("shortid");

//FUNCTION PUBLIC
function get(req, res) {
  try {
    var id = req.params.id;

    let filter = {};
    if (id) {
      filter._id = id;
    } else {
      const claim = JSON.parse(req.params.isClaim.toLowerCase());
      filter.claim = { $exists: claim };
    }

    pqrModel
      .find(filter)
      .populate("idPqrType", "_id name slug")
      .sort({ createdAt: "desc" })
      .exec((err, stored) => {
        if (err) {
          return response.error(res, global.code.g101, "g101");
        }

        if (!stored) {
          return response.error(res, global.code.g102, "g102");
        }

        if (id) {
          response.success(res, stored);
        } else {
          response.success(res, stored);
        }
      });
  } catch (e) {
    throw new Error(e);
  }
}

function post(req, res) {
  try {
    const data = ({ idPqrType, description } = req.body);

    data.settled = generateSettled();

    const modelData = new pqrModel(data);

    modelData.save((err, data) => {
      if (err) {
        return response.error(res, global.code.g101, "g101");
      } else {
        response.success(res, global.code.g201, "g201");
      }
    });
  } catch (e) {
    throw new Error(e);
  }
}

function responsepqr(req, res) {
  try {
    const { idPqr, message } = req.body;
    console.log(idPqr);
    pqrModel.findOneAndUpdate(
      { _id: idPqr },
      {
        $set: {
          "response.message": message,
          "response.date": moment.now(),
        },
      },
      (err, dataupdate) => {
        if (err) return response.error(res, global.code.g101, "g101");

        response.success(res, global.code.g201, "g201");
      }
    );
  } catch (e) {
    throw new Error(e);
  }
}

function createClaimPqr(req, res) {
  try {
    const { settled, messageUser } = req.body;

    pqrModel.findOne({ settled }, (err, data) => {
      if (err) return response.error(res, global.code.g101, "g101");

      if (!data) return response.error(res, global.code.g104, "g104");

      const dateDiff = moment().diff(data.createdAt, "days");

      if (data.response.message || dateDiff >= config.DaysForClaim) {
        claimValid(res, settled, messageUser);
      } else {
        response.success(res, global.code.g103, "g103");
      }
    });
  } catch (e) {
    throw new Error(e);
  }
}

function responseClaimPqr(req, res) {
  try {
    const { idPqr, messageResponse } = req.body;
    settled = generateSettled();
    pqrModel.findOneAndUpdate(
      { _id: idPqr },
      {
        $set: {
          "claim.messageResponse": messageResponse,
          "claim.dateReponse": moment.now(),
        },
      },
      (err, dataupdate) => {
        if (err) return response.error(res, global.code.g101, "g101");

        if (!dataupdate) return response.error(res, global.code.g101, "g101");

        response.success(res, global.code.g201, "g201");
      }
    );
  } catch (e) {
    throw new Error(e);
  }
}

//FUNCTION PRIVATE

function generateSettled() {
  var settled = Math.floor(Math.random() * 999999);
  return settled;
}

function claimValid(res, settled, messageUser) {
  const settledClaim = generateSettled();
  pqrModel.update(
    { settled },
    {
      $set: {
        "claim.settled": settledClaim,
        "claim.messageUser": messageUser,
        "claim.created": moment.now(),
      },
    },
    (err, dataupdate) => {
      if (err) return response.error(res, global.code.g101, "g101");

      if (!dataupdate) return response.error(res, global.code.g101, "g101");

      response.success(res, global.code.g202, "g202");
    }
  );
}

module.exports = {
  get,
  post,
  responsepqr,
  createClaimPqr,
  responseClaimPqr,
};
