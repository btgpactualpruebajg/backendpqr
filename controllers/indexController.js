const response = require("../helpers/responseHelper");

function getIndex(req, res) {
  try {
    let textResponse = global.code.g100;
    response.success(res, textResponse);
  } catch (e) {
    throw new Error(e);
  }
}

module.exports = {
  getIndex,
};
