////File Content config app
const express = require("express");
const cors = require("cors");
const connectDB = require("./connection/mongo");
const { config } = require("./config/config");
const errors = require("./helpers/errorHelper");
const bodyParse = require("body-parser");
const corsOptions = { origin: config.cors };

//Routes
const indexRoutes = require("./routes/indexRoutes");
const pqrTypeRoutes = require("./routes/pqrTypeRoutes");
const pqrRoutes = require("./routes/pqrRoutes");
const app = express();

//enabled cors
app.use(cors(corsOptions));

//config enabled json
app.use(bodyParse.urlencoded({ extended: true }));
app.use(bodyParse.json());

//Setting
connectDB();
app.set("port", config.port);

//Config Routes
app.use("/", indexRoutes);
app.use("/api/pqrtype", pqrTypeRoutes);
app.use("/api/pqr", pqrRoutes);
app.use(errors);
module.exports = app;
